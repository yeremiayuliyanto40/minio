﻿using JustSaying.AwsTools.QueueCreation;
using Minio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace CobaMinIO
{
    class Program
    {
        private const int MB = 1024 * 1024;

        static void Main(string[] args)
        {
            var endpoint = "localhost:9000";
            var accessKey = "2QGEP66N5LWI1AYP09RE";
            var secretKey = "n+qk742HZqKkHHBsAzwL9bkC8ME4oye+DpxIotgQ";

            try
            {
                var minio = new MinioClient(endpoint, accessKey, secretKey);
                Program program = new Program();
                //program.uploadFile(minio).Wait();
                //program.removeFile(minio).Wait();
                program.getFile(minio).Wait();
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }

        public async Task getFile(MinioClient minio)
        {
            var bucketName = "mypicture";
            var objectName = "kon.jpeg";
            var fileName = "kon";
            //ServerSideEncryption sse = null;

            try
            {
                Console.Out.WriteLine("Running example for API: GetObjectAsync");
                await minio.GetObjectAsync(bucketName, objectName,
                (stream) =>
                {

                });
                Console.WriteLine("Downloaded the file " + fileName + " in bucket " + bucketName);
                Console.Out.WriteLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("[Bucket]  Exception: {0}", e);
            }
        }

        public async Task uploadFile(MinioClient minio)
        {
            var bucketName = "mypicture";
            var location = "us-east-1";
            var objectName = "cobaa.jpeg";
            var filePath = "C:\\Users\\hp\\Downloads\\cobaa.jpeg";
            var contentType = "application/zip";
            
            try
            {
                bool found = await minio.BucketExistsAsync(bucketName);
                if (!found)
                {
                    await minio.MakeBucketAsync(bucketName, location);
                }
                await minio.PutObjectAsync(bucketName, objectName, filePath, contentType);
                Console.Out.WriteLine("Successfully uploaded " + objectName);
            }
            catch (Exception e)
            {
                Console.WriteLine("File Upload Error: {0}", e.Message);
            }
        }

        public async Task removeFile(MinioClient minio)
        {
            var bucketName = "mypicture";
            var objectName = "coba.jpeg";

            try
            {
                Console.Out.WriteLine("Running example for API: RemoveObjectAsync");
                await minio.RemoveObjectAsync(bucketName, objectName);
                Console.Out.WriteLine("Removed object " + objectName + " from bucket " + bucketName + " successfully");
                Console.Out.WriteLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("[Bucket-Object]  Exception: {0}", e);
            }
        }

        //public async Task updateFile(MinioClient minio)
        //{
        //    var bucketName = "mypicture";
        //    var objectName = "kon.jpeg";
        //    var fileName = "kon";
        //    ServerSideEncryption sse = null;

        //    try
        //    {
        //        Console.Out.WriteLine("Running example for API: GetObjectAsync");
        //        File.Delete(fileName);
        //        await minio.GetObjectAsync(bucketName, objectName, fileName, sse: sse);
        //        Console.WriteLine("Downloaded the file " + fileName + " from bucket " + bucketName);
        //        Console.Out.WriteLine();
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("[Bucket]  Exception: {0}", e);
        //    }
        //}
    }
}
